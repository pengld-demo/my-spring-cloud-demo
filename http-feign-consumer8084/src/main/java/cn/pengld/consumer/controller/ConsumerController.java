package cn.pengld.consumer.controller;

import cn.pengld.consumer.client.LoadBalanceDemoClient;
import cn.pengld.consumer.client.ProductClient;
import cn.pengld.consumer.client.UserClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsumerController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerController.class);

    @Autowired
    UserClient userClient;
    @Autowired
    ProductClient productClient;
    @Autowired
    LoadBalanceDemoClient loadBalanceDemoClient;

    @GetMapping("/getUserNameById")
    public String getUserNameById(@RequestParam("userId") String userId){
        LOGGER.info("userId:{}",userId);
        return userClient.getUserNameById(userId);
    }

    @GetMapping("/getProductNameById")
    public String getProductNameById(@RequestParam("productId") String productId){
        return productClient.getProductNameById(productId);
    }

    @GetMapping("/getProviderInfo")
    public String getProviderInfo(){
        return loadBalanceDemoClient.getProviderInfo();
    }
}
