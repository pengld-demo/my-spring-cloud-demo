package cn.pengld.consumer.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @InterfaceName LoadBalanceDemoClient
 * @Description
 * @Author pengbin
 * @Date 2021/9/26 11:12
 * @Version 1.0
 */
@FeignClient(name="http://${env.app.name.loadBalanceDemo}",contextId = "LoadBalanceDemoClient",path = "/loadBalanceDemo")
public interface LoadBalanceDemoClient {

    @GetMapping("/getProviderInfo")
    String getProviderInfo();
}
