package cn.pengld.consumer.interceptor;

import cn.pengld.consumer.filter.TraceIdFilter;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

/**
 * @ClassName OpenFeignRequestInterceptor
 * @Description
 * @Author pengbin
 * @Date 2021/9/23 23:31
 * @Version 1.0
 */
@Component
public class OpenFeignRequestInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {
        String traceId = MDC.get(TraceIdFilter.MDC_TRACE_ID);
        requestTemplate.header(TraceIdFilter.MDC_TRACE_ID, traceId);
    }
}