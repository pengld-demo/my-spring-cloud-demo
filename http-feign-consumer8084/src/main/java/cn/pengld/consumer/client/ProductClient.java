package cn.pengld.consumer.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name="http://${env.app.name.product}",contextId = "ProductClient",path = "/product")
public interface ProductClient {

    @GetMapping("/getProductNameById")
    String getProductNameById(@RequestParam("productId") String productId);

}
