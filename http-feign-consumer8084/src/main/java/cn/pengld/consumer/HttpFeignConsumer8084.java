package cn.pengld.consumer;

import cn.pengld.consumer.filter.TraceIdFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@EnableEurekaClient
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@ServletComponentScan
public class HttpFeignConsumer8084 {
    public static void main(String[] args) {
        SpringApplication.run(HttpFeignConsumer8084.class, args);
    }

    /**
     *  注册 过滤器的方式
     *  另一种为在启动类上增加注解：@ServletComponentScan，过滤器上增加注解 @WebFilter
     * @return
     */
//    @Bean
//    public FilterRegistrationBean<TraceIdFilter> registerAuthFilter() {
//        FilterRegistrationBean<TraceIdFilter> registration = new FilterRegistrationBean<>();
//        registration.setFilter(new TraceIdFilter());
//        registration.addUrlPatterns("/*");
//        registration.setName("traceIdFilter");
//        //值越小，Filter越靠前
//        registration.setOrder(1);
//        return registration;
//    }
}
