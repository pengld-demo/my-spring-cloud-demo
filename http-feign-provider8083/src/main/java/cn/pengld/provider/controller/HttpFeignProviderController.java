package cn.pengld.provider.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
public class HttpFeignProviderController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpFeignProviderController.class);

    @Value("${server.port}")
    private String serverPort;
    @Value("${spring.application.name}")
    private String springApplicationName;

    @GetMapping("/user/getUserNameById")
    public String getUserNameById(@RequestParam("userId") String userId){
        LOGGER.info("userId:{}",userId);
        return "张学友";
    }

    @GetMapping("/product/getProductNameById")
    public String getProductNameById(@RequestParam("productId") String productId){
        return "iphone 13";
    }

    @GetMapping("/loadBalanceDemo/getProviderInfo")
    public String getProviderInfo(){
        return "当前Provider-springApplicationName: " + springApplicationName + ",端口为：" + serverPort;
    }
}
