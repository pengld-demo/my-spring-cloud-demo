package cn.pengld.provider.util;

import cn.hutool.core.util.IdUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @ClassName ThreadLocalUtils
 * @Description
 * @Author pengbin
 * @Date 2021/9/23 17:15
 * @Version 1.0
 */
public class ThreadLocalUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ThreadLocalUtils.class);
    private static ThreadLocal<String> traceIdThreadLocal = new ThreadLocal();

    private ThreadLocalUtils() {
    }

    public static void setTraceId(String traceId) {
        traceIdThreadLocal.set(traceId);
    }

    public static String getTraceId() {
        if (null == traceIdThreadLocal.get()) {
            traceIdThreadLocal.set(IdUtil.fastSimpleUUID());
        }
        return (String)traceIdThreadLocal.get();
    }

    public static void removeTraceId() {
        traceIdThreadLocal.remove();
    }
}
