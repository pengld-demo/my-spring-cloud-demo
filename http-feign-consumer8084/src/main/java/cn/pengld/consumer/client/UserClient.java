package cn.pengld.consumer.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name="http://${env.app.name.user}",contextId = "UserClient",path = "/user")
public interface UserClient {

    @GetMapping("/getUserNameById")
    String getUserNameById(@RequestParam("userId") String userId);

}
