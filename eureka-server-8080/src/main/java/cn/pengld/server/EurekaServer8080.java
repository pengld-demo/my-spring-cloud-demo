package cn.pengld.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @ClassName EurekaServer8080
 * @Description
 * @Author pengbin
 * @Date 2021/9/16 14:12
 * @Version 1.0
 */
@EnableEurekaServer
@EnableDiscoveryClient
@SpringBootApplication
public class EurekaServer8080 {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServer8080.class, args);
    }
}
