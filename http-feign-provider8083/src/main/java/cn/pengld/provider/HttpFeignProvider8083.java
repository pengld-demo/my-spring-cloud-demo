package cn.pengld.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @ClassName HttpFeignProvider8083
 * @Description
 * @Author pengbin
 * @Date 2021/9/17 10:46
 * @Version 1.0
 */
@EnableEurekaClient
@EnableDiscoveryClient
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@ServletComponentScan
public class HttpFeignProvider8083 {
    public static void main(String[] args) {
        SpringApplication.run(HttpFeignProvider8083.class, args);
    }
}
