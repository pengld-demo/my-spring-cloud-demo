package cn.pengld.provider.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HeartBeatController {
    @Value("${server.port}")
    private String serverPort;
    @Value("${spring.application.name}")
    private String springApplicationName;

    @RequestMapping("/heartBeat")
    public String heartBeat(){
        return "success.The server is " + springApplicationName + ". port is " + serverPort;
    }
}
